from django.urls import path
from . import views

urlpatterns = [
    #Leave as empty string for base url
	path('', views.index, name="home"),
	path('service/<slug:category_slug>/<slug:service_slug>/', views.service, name="service"),
	path('contact/', views.contact, name="contact"),
	path('about/', views.about, name="about"),
	path('blog/<slug:category_slug>/<slug:blog_slug>/', views.blog, name="blog"),

]