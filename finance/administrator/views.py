from django.shortcuts import render, get_object_or_404
from . models import *
# Create your views here.
def index(request):
    if request.method == "POST":
        email = request.POST['email']
        print(email)
        ins = Subscription(email=email)
        ins.save()
        print("The email as been written to the db")
    context = {'service':Service.objects.all(), 'blog': Blog.objects.all()[0:8]}
    return render(request, 'Administrator/index.html', context)

def service(request, category_slug, service_slug):
    model = get_object_or_404(Service, category__slug=category_slug, slug=service_slug)
    return render(request, 'administrator/service.html', {'service':model, 'category_slug':category_slug, 'slug':service_slug})

def about(request):
    context = {'blog': Blog.objects.all()}
    return render(request, 'Administrator/about.html', context)

def blog(request, category_slug, blog_slug):
    model = get_object_or_404(Blog, category__slug=category_slug, slug=blog_slug)
    return render(request, 'Administrator/single-blog.html', {'blog': model, 'category_slug': category_slug, 'slug':blog_slug})

def contact(request):
    context = {}
    return render(request, 'Administrator/contact-us.html', context)