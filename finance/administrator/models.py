from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=200, null=True)
    slug = models.CharField(max_length=200, null=True)

    def __str__(self):
        return self.title


class Service(models.Model):
    category=models.ForeignKey(Category,related_name='service',on_delete=models.CASCADE,null=True, blank=True)
    title = models.CharField(max_length=200, null=True)
    date_added = models.DateTimeField(auto_now_add=True)
    paragraph_1 = models.TextField(max_length=1000, null=True)
    paragraph_2 = models.TextField(max_length=1000, null=True)
    paragraph_3 = models.TextField(max_length=1000, null=True)
    slug=models.SlugField(max_length=255,null=True, blank=True)
    image = models.ImageField(upload_to='uploads/Service/',null=True, blank=True)


    def __str__(self):
        return self.title

    @property
    def getImageURL(self):
        if self.image.url and hasattr(self.image,'url'):
           return self.image.url
        else:
            return 'static/img/frontpage.jpg'


class Blog(models.Model):
    category=models.ForeignKey(Category,related_name='blog',on_delete=models.CASCADE,null=True, blank=True)
    title = models.CharField(max_length=200, null=True)
    date_added = models.DateTimeField(auto_now_add=True)
    paragraph_1 = models.TextField(max_length=1000, null=True)
    paragraph_2 = models.TextField(max_length=1000, null=True)
    paragraph_3 = models.TextField(max_length=1000, null=True)
    comments = models.CharField(max_length=10, null=True)
    slug=models.SlugField(max_length=255,null=True, blank=True)
    image = models.ImageField(upload_to='uploads/Blog/',null=True, blank=True)


    def __str__(self):
        return self.title

    @property
    def getImageURL(self):
        if self.image.url and hasattr(self.image,'url'):
           return self.image.url
        else:
            return 'static/img/frontpage.jpg'

class Subscription(models.Model):
    email = models.EmailField(max_length=200, null=True)

    def __str__(self):
        return self.email